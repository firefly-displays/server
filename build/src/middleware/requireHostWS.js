"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var requireHostWS = function (req, res, next) {
    var _a;
    var hostId = req.query.host;
    var connection = (_a = global.hosts[hostId]) === null || _a === void 0 ? void 0 : _a.connection;
    if (!connection || !connection.OPEN) {
        console.log('host is offline');
        return res.sendStatus(400);
    }
    res.locals.ws = connection;
    return next();
};
exports.default = requireHostWS;
