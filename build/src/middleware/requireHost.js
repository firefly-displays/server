"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var requireHost = function (req, res, next) {
    var host = res.locals.host;
    if (!host) {
        return res.sendStatus(403);
    }
    return next();
};
exports.default = requireHost;
