"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var requireUser_1 = __importDefault(require("./middleware/requireUser"));
var auth_controller_1 = require("./controllers/auth.controller");
var user_controller_1 = require("./controllers/user.controller");
var requireHost_1 = __importDefault(require("./middleware/requireHost"));
var host_controller_1 = require("./controllers/host.controller");
var deserializeUser_1 = require("./middleware/deserializeUser");
var requireHostWS_1 = __importDefault(require("./middleware/requireHostWS"));
var displays_controller_1 = require("./controllers/displays.controller");
var shedulers_controller_1 = require("./controllers/shedulers.controller");
var media_controller_1 = require("./controllers/media.controller");
function routes(app) {
    app.get("/healthcheck", function (req, res) { return res.sendStatus(200); });
    app.get("/api/me", requireUser_1.default, user_controller_1.getCurrentUser);
    app.get("/api/oauth/google", auth_controller_1.googleOauthHandler);
    app.get("/api/logout", auth_controller_1.logout);
    app.post("/api/authHost", auth_controller_1.authHost);
    app.get('/api/host/code', deserializeUser_1.deserializeHost, requireHost_1.default, host_controller_1.getHostConnectionCode);
    app.post('/api/connectHost', requireUser_1.default, user_controller_1.connectHost);
    app.get('/api/hosts', requireUser_1.default, user_controller_1.getHosts);
    app.get('/api/displays', requireUser_1.default, requireHostWS_1.default, displays_controller_1.getDisplays);
    app.get('/api/schedulers', requireUser_1.default, requireHostWS_1.default, shedulers_controller_1.getShedulers);
    app.post('/api/schedulers', requireUser_1.default, requireHostWS_1.default, shedulers_controller_1.postScheduler);
    app.get('/api/media', requireUser_1.default, requireHostWS_1.default, media_controller_1.getMedia);
    app.get('/api/mediathumb', requireUser_1.default, requireHostWS_1.default, media_controller_1.getThumb);
}
exports.default = routes;
