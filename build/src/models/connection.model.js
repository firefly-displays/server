"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var connectionCodeSchema = new mongoose_1.default.Schema({
    host_id: {
        type: String,
        required: true,
        ref: 'host'
    },
    code: {
        type: String,
        required: true,
        unique: true
    },
    expiresAt: {
        type: Date,
        default: function () { return new Date(+new Date() + 15 * 60 * 1000); },
        expires: 0
    }
});
var ConnectionModel = mongoose_1.default.model('connectionModel', connectionCodeSchema);
exports.default = ConnectionModel;
