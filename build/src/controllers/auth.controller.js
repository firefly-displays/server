"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authHost = exports.reIssueAccessToken = exports.logout = exports.googleOauthHandler = void 0;
var config_1 = __importDefault(require("config"));
var jwt_1 = require("../utils/jwt");
var user_controller_1 = require("./user.controller");
var logger_1 = __importDefault(require("../utils/logger"));
var node_path_1 = __importDefault(require("node:path"));
var host_controller_1 = require("./host.controller");
var accessTokenCookieOptions = {
    maxAge: 900000,
    httpOnly: false,
    domain: "localhost",
    path: "/",
    sameSite: "lax",
    secure: false,
};
var refreshTokenCookieOptions = __assign(__assign({}, accessTokenCookieOptions), { maxAge: 3.154e10 });
function googleOauthHandler(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var code, _a, id_token, access_token, googleUser, accessToken, refreshToken, error_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    code = req.query.code;
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 5, , 6]);
                    return [4 /*yield*/, (0, user_controller_1.getGoogleOAuthTokens)({ code: code })];
                case 2:
                    _a = _b.sent(), id_token = _a.id_token, access_token = _a.access_token;
                    return [4 /*yield*/, (0, user_controller_1.getGoogleUser)({ id_token: id_token, access_token: access_token })];
                case 3:
                    googleUser = _b.sent();
                    if (!googleUser.verified_email) {
                        return [2 /*return*/, res.status(403).send("Google account is not verified")];
                    }
                    return [4 /*yield*/, (0, user_controller_1.findAndUpdateUser)({
                            id: googleUser.id,
                        }, {
                            email: googleUser.email,
                            name: googleUser.name,
                            picture: googleUser.picture,
                        }, {
                            upsert: true,
                            new: true,
                        })];
                case 4:
                    _b.sent();
                    accessToken = (0, jwt_1.signJwt)(googleUser, { expiresIn: config_1.default.get("accessTokenTtl") });
                    refreshToken = (0, jwt_1.signJwt)(googleUser, { expiresIn: config_1.default.get("refreshTokenTtl") });
                    // set cookies
                    res.cookie("accessToken", accessToken, accessTokenCookieOptions);
                    res.cookie("refreshToken", refreshToken, refreshTokenCookieOptions);
                    // res.redirect(config.get("origin"));
                    // res.redirect('http://localhost:3000');
                    res.sendFile(node_path_1.default.join(__dirname + '/../assets/auth_end.html'));
                    return [3 /*break*/, 6];
                case 5:
                    error_1 = _b.sent();
                    logger_1.default.error(error_1, "Failed to authorize Google user");
                    return [2 /*return*/, res.redirect("".concat(config_1.default.get("origin"), "/oauth/error"))];
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.googleOauthHandler = googleOauthHandler;
var logout = function (req, res) {
    res.clearCookie('accessToken');
    res.clearCookie('refreshToken');
    res.locals.user = undefined;
    res.sendStatus(200);
};
exports.logout = logout;
function reIssueAccessToken(_a) {
    var refreshToken = _a.refreshToken;
    return __awaiter(this, void 0, void 0, function () {
        var decoded, user;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    decoded = (0, jwt_1.verifyJwt)(refreshToken).decoded;
                    if (!decoded)
                        return [2 /*return*/, false];
                    return [4 /*yield*/, (0, user_controller_1.findUser)({ decoded: decoded })];
                case 1:
                    user = _b.sent();
                    if (!user)
                        return [2 /*return*/, false];
                    return [2 /*return*/, (0, jwt_1.signJwt)(user, { expiresIn: config_1.default.get("accessTokenTtl") })];
            }
        });
    });
}
exports.reIssueAccessToken = reIssueAccessToken;
var authHost = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, id, name, os, accessToken, refreshToken;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, id = _a.id, name = _a.name, os = _a.os;
                return [4 /*yield*/, (0, host_controller_1.findAndUpdateHost)({ id: id }, { name: name, os: os }, { upsert: true, new: true, })];
            case 1:
                _b.sent();
                accessToken = (0, jwt_1.signJwt)({ id: id, name: name, os: os }, { expiresIn: "10y" });
                refreshToken = (0, jwt_1.signJwt)({ id: id, name: name, os: os }, { expiresIn: "10y" });
                res.status(200).json({ accessToken: accessToken, refreshToken: refreshToken });
                return [2 /*return*/];
        }
    });
}); };
exports.authHost = authHost;
