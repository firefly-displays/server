"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getHosts = exports.connectHost = exports.getCurrentUser = exports.findAndUpdateUser = exports.getGoogleUser = exports.getGoogleOAuthTokens = exports.findUser = exports.createUser = void 0;
var qs_1 = __importDefault(require("qs"));
var config_1 = __importDefault(require("config"));
var axios_1 = __importDefault(require("axios"));
var user_model_1 = __importDefault(require("../models/user.model"));
var logger_1 = __importDefault(require("../utils/logger"));
var connection_model_1 = __importDefault(require("../models/connection.model"));
var host_model_1 = __importDefault(require("../models/host.model"));
function createUser(input) {
    return __awaiter(this, void 0, void 0, function () {
        var e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, user_model_1.default.create(input)];
                case 1: return [2 /*return*/, _a.sent()];
                case 2:
                    e_1 = _a.sent();
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    });
}
exports.createUser = createUser;
function findUser(query) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, user_model_1.default.findOne(query).lean()];
        });
    });
}
exports.findUser = findUser;
function getGoogleOAuthTokens(_a) {
    var code = _a.code;
    return __awaiter(this, void 0, void 0, function () {
        var url, values, res, error_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    url = "https://oauth2.googleapis.com/token";
                    values = {
                        code: code,
                        client_id: config_1.default.get("googleClientId"),
                        client_secret: config_1.default.get("googleClientSecret"),
                        redirect_uri: config_1.default.get("googleOauthRedirectUrl"),
                        grant_type: "authorization_code",
                    };
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, axios_1.default.post(url, qs_1.default.stringify(values), {
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded",
                            },
                        })];
                case 2:
                    res = _b.sent();
                    return [2 /*return*/, res.data];
                case 3:
                    error_1 = _b.sent();
                    console.error(error_1.response.data.error);
                    logger_1.default.error(error_1, "Failed to fetch Google Oauth Tokens");
                    throw new Error(error_1.message);
                case 4: return [2 /*return*/];
            }
        });
    });
}
exports.getGoogleOAuthTokens = getGoogleOAuthTokens;
function getGoogleUser(_a) {
    var id_token = _a.id_token, access_token = _a.access_token;
    return __awaiter(this, void 0, void 0, function () {
        var res, error_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, axios_1.default.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=".concat(access_token), {
                            headers: {
                                Authorization: "Bearer ".concat(id_token),
                            },
                        })];
                case 1:
                    res = _b.sent();
                    return [2 /*return*/, res.data];
                case 2:
                    error_2 = _b.sent();
                    logger_1.default.error(error_2, "Error fetching Google user");
                    throw new Error(error_2.message);
                case 3: return [2 /*return*/];
            }
        });
    });
}
exports.getGoogleUser = getGoogleUser;
function findAndUpdateUser(query, update, options) {
    if (options === void 0) { options = {}; }
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, user_model_1.default.findOneAndUpdate(query, update, options)];
        });
    });
}
exports.findAndUpdateUser = findAndUpdateUser;
function getCurrentUser(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, res.send(res.locals.user)];
        });
    });
}
exports.getCurrentUser = getCurrentUser;
function connectHost(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var code, record, host;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    code = req.body.code;
                    return [4 /*yield*/, connection_model_1.default.findOne({ code: code }).exec()];
                case 1:
                    record = _a.sent();
                    if (!record) {
                        res.status(401).send('Wrong code');
                        return [2 /*return*/];
                    }
                    return [4 /*yield*/, host_model_1.default.findOne({ id: record["host_id"] }).exec()];
                case 2:
                    host = _a.sent();
                    if (!host) {
                        res.status(500).send('Host is missing');
                        return [2 /*return*/];
                    }
                    return [4 /*yield*/, user_model_1.default.findOneAndUpdate({ id: res.locals.user.id }, { $addToSet: { hosts: host._id } }, { new: true, useFindAndModify: false }).exec().catch(function () {
                            res.status(500).send('An error occurred while connecting');
                        })];
                case 3:
                    _a.sent();
                    res.status(200).send();
                    return [2 /*return*/];
            }
        });
    });
}
exports.connectHost = connectHost;
function getHosts(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, user_model_1.default.findOne({ id: res.locals.user.id })
                        .populate('hosts')
                        .exec()
                        .catch(function () { return res.status(500).send(); })
                    // @ts-ignore
                ];
                case 1:
                    user = _a.sent();
                    // @ts-ignore
                    res.status(200).json(user === null || user === void 0 ? void 0 : user.hosts);
                    return [2 /*return*/];
            }
        });
    });
}
exports.getHosts = getHosts;
