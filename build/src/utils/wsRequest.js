"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.wsRequest = void 0;
// @ts-ignore
var uuid_1 = require("uuid");
function wsRequest(res, payload) {
    var requestId = (0, uuid_1.v4)();
    var ws = res.locals.ws;
    payload.requestId = requestId;
    ws.addEventListener('message', function (msg) {
        var data = JSON.parse(msg.data);
        if (data.requestId == requestId) {
            console.log(data);
            try {
                res.status(200).send(data.payload);
            }
            catch (e) { }
        }
    });
    ws.send(JSON.stringify(payload));
}
exports.wsRequest = wsRequest;
