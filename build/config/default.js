"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    port: 1337,
    origin: "http://localhost:3000",
    dbUri: "mongodb://localhost:27017/firefly_displays",
    saltWorkFactor: 10,
    accessTokenTtl: "15m",
    refreshTokenTtl: "1y",
    publicKey: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1KWbYQRiX7wwoOnCTSZ0\nZJK1kxGcwLeMTv3zJ1CZ16lt+xH+mMJtOc0shmjPquJz6F3AFRWJo1wCcc9yWpvd\neTgKQpXC1afk9ptX/zKmLmRC/Qkf0EYNQTfrt0Ia9cen8HqOwdWiXh8Tqje03E0y\nNcflHG4Y5z3+fCyFa9RhAK3qyJy2uXOXTTNBUZRPaLkle+mBx5jBwmt9RE42szsO\nEev9K7E21AvVo+cW+PRJOgwwEgIGd1vojadUdy54marJDvkxLg0XBz1AKVpBkJft\nAkSGQi6c+tcziYLLjc55wwhWDsTHJF06bYGiO4G4BnVKBwkLw3CoND98UdTTcJKC\nlQIDAQAB\n-----END PUBLIC KEY-----",
    privateKey: "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA1KWbYQRiX7wwoOnCTSZ0ZJK1kxGcwLeMTv3zJ1CZ16lt+xH+\nmMJtOc0shmjPquJz6F3AFRWJo1wCcc9yWpvdeTgKQpXC1afk9ptX/zKmLmRC/Qkf\n0EYNQTfrt0Ia9cen8HqOwdWiXh8Tqje03E0yNcflHG4Y5z3+fCyFa9RhAK3qyJy2\nuXOXTTNBUZRPaLkle+mBx5jBwmt9RE42szsOEev9K7E21AvVo+cW+PRJOgwwEgIG\nd1vojadUdy54marJDvkxLg0XBz1AKVpBkJftAkSGQi6c+tcziYLLjc55wwhWDsTH\nJF06bYGiO4G4BnVKBwkLw3CoND98UdTTcJKClQIDAQABAoIBADIiNSbDXNoLST+2\nvOtphIltmUdqIJXC8F5zOs9rDkotFNPbozWqqQu2lqe9sDAuoXsIF1AhOsj72Uux\nx9ka5DU/BBQ8sKmkorONXkq5SzqmwrBbZVe6HEfjAjy7ZhXKE4r7qgqC7nvYQc+g\nwJAr1wmc7iQsxMxH7maiLHaGpLP+3nzEwyuM/8vqQvb7P1QhHUcEyauJd9THkuJj\ns9FgqLYcU/b+33aTXKGSaX9nnj6bwM4bZP6yKZgAd894UZYZWNA8XZCpFZdBMLNh\nfpgG7RrvGRLDd0YDzCmJlUdSEZL2UUu3aAvwpF3clovme0+YwEJ/XIC05wMaQ/rQ\nV9Q8caECgYEA/LNVLck+bHHucZbTlWBhSWwjfxwf0S1xhDzwISHRAXkOZ7Du4fiH\nyC6EeyPUjvqbkXtgb/V5wdmVSbq8AsI3vgW9yr2eHWNGaA2wlIkdeZC7UeD+Z2Ps\nOWh882TUs1xIRef98LlY3SrPt54JvksHT0TR7ilhNwHm5+/8CKVsPGkCgYEA12xk\neyopWSq6+uLtjlGRk9qWZc90+Ur5jCye0GBpzJjINZNdtA19RcrlAHCEnnVrjypa\nSrzykNaqHF20zLhNPDfiz8gFOxSZvxjClCr1nnlnEsfv8IHoZ/CDs6lGxUYx5qnS\nbEID4PSKxl8nbriYafIfAinJEFZLifFXYagiv00CgYA7uxwANZhmw8tr2A3tOhnk\n5AXD7t7qb8ZsTocoHbkskvO/uMfObBOUUsKH5OFCZqRNEA+shyaflpG+GsCEGmpY\npTzGZmmwYp3qzImXoo1wBbDxWrBnuevFahhW49ePCdofhAW3V+hUZuEwcs+0xkFV\nEFxa4jcTbISrAEPfu1JJOQKBgQCP6eO+vE3fOcKsNMnK2AdPJSpwXYWhvUq1MGHw\nh1hmzHKECC0DSje0VnViM8kFKyBuBjdH0q6hwCboyFkuorlGT3eAo/mKnJqRpXLo\nFUL9WdFYJKXQoJioR29UqPOUeu5/UFg45mk88WF8YLkxKLT5ojzA7zVZaq0QPGDJ\n5s8/yQKBgQDpbqk9AJocks6sXSxcVqrVr8dx9RMD52q3aBtxNjCtb3RiZgvMVZ8+\ntgShD1JCu0KT/K5REqbU5fqloEXeKV5yER4UgZxv9HFj5pqOFlDp9fiH15ne7J1A\n1wY71SDjO5Lh5FH7EMvb/AafQE4+uQvUDO5CZtgaoiYmXt9Ql1d5OQ==\n-----END RSA PRIVATE KEY-----",
    googleClientId: "840570424789-f2fbnv7et41k789e6kdn3prsek41uj3c.apps.googleusercontent.com",
    googleClientSecret: "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",
    googleOauthRedirectUrl: "http://localhost:1337/api/oauth/google",
};
