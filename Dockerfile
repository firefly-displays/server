# Указываем базовый образ
FROM node:14

# Создаем директорию приложения
WORKDIR /usr/src/app

# Копируем файлы package.json и package-lock.json (если есть)
COPY package*.json ./

# Устанавливаем зависимости приложения, включая TypeScript
RUN npm install

# Копируем исходный код приложения
COPY . .

# Компилируем TypeScript в JavaScript
RUN npm run build

# Открываем порт, на котором будет работать приложение
EXPOSE 3000

# Запускаем скомпилированное приложение
CMD ["node", "build/src/app.js"]
