import mongoose from "mongoose";

export interface HostDocument extends mongoose.Document {
    id: string,
    name: string,
    os: string
}

const hostSchema = new mongoose.Schema(
    {
        id: { type: String, required: true, unique: true },
        name: { type: String, required: true },
        os: { type: String, required: true },
    },
    {
        timestamps: true,
    }
);

const HostModel = mongoose.model<HostDocument>("host", hostSchema);

export default HostModel;