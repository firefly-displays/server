import mongoose from "mongoose";

const connectionCodeSchema = new mongoose.Schema({
    host_id: {
        type: String,
        required: true,
        ref: 'host'
    },
    code: {
        type: String,
        required: true,
        unique: true
    },
    expiresAt: {
        type: Date,
        default: () => new Date(+new Date() + 15 * 60 * 1000),
        expires: 0
    }
});

const ConnectionModel = mongoose.model('connectionModel', connectionCodeSchema);

export default ConnectionModel