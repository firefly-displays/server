import mongoose, { Schema } from "mongoose";

export interface UserDocument extends mongoose.Document {
    id: string,
    email: string,
    name: string,
    picture: string,
}

const userSchema = new mongoose.Schema(
    {
        id: { type: String, required: true, unique: true },
        email: { type: String, required: true },
        name: { type: String, required: true },
        picture: { type: String },
        hosts: [{
            type: Schema.Types.ObjectId,
            ref: 'host'
        }]
    },
    {
        timestamps: true,
    }
);

const UserModel = mongoose.model<UserDocument>("user", userSchema);

export default UserModel;