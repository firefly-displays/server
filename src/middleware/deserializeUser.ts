import { get } from "lodash";
import { Request, Response, NextFunction } from "express";
import { verifyJwt } from "../utils/jwt";
import { reIssueAccessToken } from "../controllers/auth.controller";

const deserialize = async (
    req: Request,
    res: Response,
    next: NextFunction,
    callback: (obj: any) => void
) => {
    const accessToken =
        get(req, "cookies.accessToken") ||
        get(req, "headers.authorization", "").replace(/^Bearer\s/, "");

    const refreshToken =
        get(req, "cookies.refreshToken") || get(req, "headers.x-refresh");


    const updateAccessToken = async () => {
        const newAccessToken = await reIssueAccessToken({ refreshToken });

        if (newAccessToken) {
            res.setHeader("x-access-token", newAccessToken);

            res.cookie("accessToken", newAccessToken, {
                maxAge: 900000, // 15 mins
                httpOnly: false,
                domain: "localhost",
                path: "/",
                sameSite: "lax",
                secure: false,
            });
        }

        const result = verifyJwt(newAccessToken as string);

        callback(result.decoded);
    }

    if (!refreshToken) {
        return next();
    }

    if (!accessToken) {
        await updateAccessToken();
        return next();
    }
    
    const { decoded, expired } = verifyJwt(accessToken);

    if (decoded) {
        callback(decoded);
        return next();
    }

    if (expired) {
        await updateAccessToken();
    }

    return next();
};


export const deserializeUser = async(req: Request, res: Response, next: NextFunction) => {
    await deserialize(req, res, next, (obj) => {
        res.locals.user = obj
    });
}

export const deserializeHost = async(req: Request, res: Response, next: NextFunction) => {
    await deserialize(req, res, next, (obj) => {
        res.locals.host = obj
    });
}

