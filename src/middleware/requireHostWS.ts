import { Request, Response, NextFunction } from "express";
import {GlobalType} from "../app";

const requireHostWS = (req: Request, res: Response, next: NextFunction) => {
    const hostId = req.query.host as string;
    const connection = (global as GlobalType).hosts[hostId]?.connection

    if (!connection || !connection.OPEN) {
        console.log('host is offline')
        return res.sendStatus(400);
    }

    res.locals.ws = connection

    return next();
};

export default requireHostWS;