import { Request, Response, NextFunction } from "express";

const requireHost = (req: Request, res: Response, next: NextFunction) => {
    const host = res.locals.host;

    if (!host) {
        return res.sendStatus(403);
    }

    return next();
};

export default requireHost;