// @ts-ignore
import {v4 as uuidv4} from 'uuid';
import {Response} from "express";

export function wsRequest(res: Response, payload: any) {
    const requestId = uuidv4();
    const ws = res.locals.ws

    payload.requestId = requestId

    ws.addEventListener('message', (msg: any) => {
        const data = JSON.parse(msg.data)
        if (data.requestId == requestId) {
            console.log(data)
            try {
                res.status(200).send(data.payload)
            } catch (e) {}
        }
    })

    ws.send(JSON.stringify(payload))
}
