import {Request, Response} from "express";
import {wsRequest} from "../utils/wsRequest";

export async function getDisplays(req: Request, res: Response) {
    const msg = {
        type: 'get',
        entity: 'display',
        id: '*'
    }
    wsRequest(res, msg)
}
