import {Request, Response} from "express";
import {wsRequest} from "../utils/wsRequest";

export async function getShedulers(req: Request, res: Response) {
    const msg = {
        type: 'get',
        entity: 'scheduler',
        id: '*'
    }
    wsRequest(res, msg)
}

export async function postScheduler(req: Request, res: Response) {
    const msg = {
        type: 'post',
        entity: 'scheduler',
        payload: JSON.stringify({
            name: req.body.name
        })

    }
    console.log(msg)

    wsRequest(res, msg)
}

