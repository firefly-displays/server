import {Request, Response} from "express";
import {wsRequest} from "../utils/wsRequest";

export async function getMedia(req: Request, res: Response) {
    const msg = {
        type: 'get',
        entity: 'content',
        id: '*'
    }
    wsRequest(res, msg)
}

export async function getThumb(req: Request, res: Response) {
    const msg = {
        type: 'get',
        entity: 'img',
        id: req.query.id
    }
    wsRequest(res, msg)
}