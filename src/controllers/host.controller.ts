import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import HostModel, { HostDocument } from "../models/host.model";
import {Request, Response} from "express";
import ConnectionModel from "../models/connection.model";

export async function findAndUpdateHost(
    query: FilterQuery<HostDocument>,
    update: UpdateQuery<HostDocument>,
    options: QueryOptions = {}
) {
    return HostModel.findOneAndUpdate(query, update, options);
}

async function generateUniqueCode() {
    while (true) {
        const code = Math.floor(100000 + Math.random() * 900000).toString();
        const codeExists = await ConnectionModel.findOne({ code });
        if (!codeExists) {
            return code;
        }
    }
}

export async function getHostConnectionCode(req: Request, res: Response) {
    try {
        const hostId = res.locals.host.id;
        const code = await generateUniqueCode();

        ConnectionModel.deleteMany({host_id: hostId})
        const newConnectionCode = await ConnectionModel.create({
            host_id: hostId,
            code
        });
        res.status(200).send(newConnectionCode.code);
    } catch (error) {
        console.error(error);
        res.status(500).send('An error occurred while generating the connection code.');
    }
}