import { GlobalType } from "../app";
import logger from "../utils/logger";
import { IncomingMessage } from 'http';
import {verifyJwt} from "../utils/jwt";
import {get} from "lodash";
import cookie from "cookie";
import * as url from "url";
import userModel from "../models/user.model";
import UserModel from "../models/user.model";

export const verifyClient = (info: { origin: string; secure: boolean; req: IncomingMessage }, callback: (res: boolean, code?: number, message?: string, headers?: { [key: string]: string }) => void) => {
    const base64Credentials = info.req.headers.authorization?.split(' ')[1];
    const credentials = Buffer.from(base64Credentials ?? '', 'base64').toString();
    const token= credentials.split(':')[1] ?? cookie.parse(info.req.headers.cookie || '').accessToken;

    if (!token) {
        callback(false, 401, 'Unauthorized');
    } else {
        const { decoded} = verifyJwt(token)
        if (decoded) {
            callback(true, 200);
        }
        else {
            callback(false, 401, 'Unauthorized');
        }
    }
};


export function onWSConnection(ws: WebSocket, req: IncomingMessage) {
    // console.log('Someone connected')
    const requUrl = url.parse(req.url ?? '', true)

    if (requUrl.pathname === '/host') onHostConnection(ws, req)
    if (requUrl.pathname === '/client') onClientConnection(ws, req)
}

const getWSAuthJWTData = (req: IncomingMessage) => {
    const base64Credentials = req.headers.authorization?.split(' ')[1];
    // @ts-ignore
    const credentials = Buffer.from(base64Credentials ?? '', 'base64').toString();
    const token= credentials.split(':')[1] ?? cookie.parse(req.headers.cookie || '').accessToken;
    return verifyJwt(token).decoded
}

export function onHostConnection(ws: WebSocket, req: IncomingMessage) {
    const data = getWSAuthJWTData(req) as { id: string }

    if (data && data.id) {
        (global as GlobalType).hosts[data.id] = {
            // @ts-ignore
            connection: ws,
            client: null
        }
    }

    ws.onmessage = (event: MessageEvent) => {
        // console.log(`sending message ${event.data} to client`);
        (global as GlobalType).hosts[data.id].client?.send(event.data)
    };

    ws.onclose = () => {
        const hosts = (global as GlobalType).hosts
        Object.keys(hosts).forEach((hostID: string) => {
            try {
                // @ts-ignore
                if (hosts[hostID].connection === ws) {
                    hosts[hostID].connection.close();
                    hosts[hostID].client?.close();
                    delete hosts[hostID];
                    logger.info(`host ${hostID} disconnected`);
                    console.log(`host ${hostID} disconnected`)
                }
            }
            catch (e) {
                console.log(e)
            }
        })
    };
}

export async function onClientConnection(ws: WebSocket, req: IncomingMessage) {
    const user = getWSAuthJWTData(req) as { id: string }
    const reqUrl = url.parse(req.url ?? '', true)
    const hostId = reqUrl.query.hostId as string

    const canConnect = await UserModel
        .findOne({
            id: user.id,
            // hosts: { $elemMatch: { id: hostId } }
        })

    if (!canConnect) {
        ws.close()
        return
    }

    // @ts-ignore
    (global as GlobalType).hosts[hostId]?.client = ws

    ws.onmessage = (event: MessageEvent) => {
        Object.values((global as GlobalType).hosts)
            // @ts-ignore
            .filter(h => h.client && h.client === ws)
            .forEach(h => {
                // console.log(`sending message ${event.data} to host`)
                h.connection?.send(event.data)
            })
    };
}