import {get} from "lodash";
import config from "config";
import {signJwt, verifyJwt} from "../utils/jwt";
import {findAndUpdateUser, findUser, getGoogleOAuthTokens, getGoogleUser} from "./user.controller";
import {CookieOptions} from "express";
import log from "../utils/logger";
import { Request, Response } from "express";
import path from "node:path";
import { findAndUpdateHost } from "./host.controller";


const accessTokenCookieOptions: CookieOptions = {
    maxAge: 900000, // 15 mins
    httpOnly: false,
    domain: "oneren.space",
    path: "/",
    sameSite: "lax",
    secure: false,
};

const refreshTokenCookieOptions: CookieOptions = {
    ...accessTokenCookieOptions,
    maxAge: 3.154e10, // 1 year
};

export async function googleOauthHandler(req: Request, res: Response) {
    const code = req.query.code as string;

    try {
        const { id_token, access_token } = await getGoogleOAuthTokens({ code });

        const googleUser = await getGoogleUser({ id_token, access_token });

        if (!googleUser.verified_email) {
            return res.status(403).send("Google account is not verified");
        }

        await findAndUpdateUser(
            {
                id: googleUser.id,
            },
            {
                email: googleUser.email,
                name: googleUser.name,
                picture: googleUser.picture,
            },
            {
                upsert: true,
                new: true,
            }
        );

        const accessToken = signJwt(googleUser, { expiresIn: config.get("accessTokenTtl") } );
        const refreshToken = signJwt(googleUser,{ expiresIn: config.get("refreshTokenTtl") });

        // set cookies
        res.cookie("accessToken", accessToken, accessTokenCookieOptions);
        res.cookie("refreshToken", refreshToken, refreshTokenCookieOptions);

        // res.redirect(config.get("origin"));
        // res.redirect('http://localhost:3000');
        res.sendFile('/usr/src/app/src/assets/auth_end.html');
    } catch (error) {
        log.error(error, "Failed to authorize Google user");
        return res.redirect(`${config.get("origin")}/oauth/error`);
    }
}

export const logout = (req: Request, res: Response) => {
    res.clearCookie('accessToken');
    res.clearCookie('refreshToken');
    res.locals.user = undefined;
    res.sendStatus(200);
}

export async function reIssueAccessToken({ refreshToken }: { refreshToken: string; }) {
    const { decoded } = verifyJwt(refreshToken);
    if (!decoded) return false;

    const user = await findUser({ decoded });

    if (!user) return false;

    return signJwt(user, { expiresIn: config.get("accessTokenTtl") });
}

export const authHost = async (req: Request, res: Response) => {
    const {id, name, os} = req.body as {
        id: string;
        name: string;
        os: string;
    };

    await findAndUpdateHost({ id }, { name, os }, { upsert: true, new: true, });

    const accessToken = signJwt({id, name, os}, { expiresIn: "10y" });
    const refreshToken = signJwt({id, name, os},{ expiresIn: "10y" });

    res.status(200).json({ accessToken, refreshToken });
}

