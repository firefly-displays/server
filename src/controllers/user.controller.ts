import {
    DocumentDefinition,
    FilterQuery,
    QueryOptions,
    UpdateQuery,
} from "mongoose";
import qs from "qs";
import config from "config";
import axios from "axios";
import UserModel, { UserDocument } from "../models/user.model";
import log from "../utils/logger";
import { Request, Response } from "express";
import ConnectionModel from "../models/connection.model";
import HostModel from "../models/host.model";



interface IGoogleTokensResult {
    access_token: string;
    expires_in: Number;
    refresh_token: string;
    scope: string;
    id_token: string;
}

interface IGoogleUserResult {
    id: string;
    email: string;
    verified_email: boolean;
    name: string;
    given_name: string;
    family_name: string;
    picture: string;
    locale: string;
}

interface IKeysPair {
    id_token: string;
    access_token: string;
}

export async function createUser(
    input: DocumentDefinition<UserDocument>
) {
    try {
        return await UserModel.create(input);
    } catch (e: any) { }
}


export async function findUser(query: FilterQuery<UserDocument>) {
    return UserModel.findOne(query).lean();
}


export async function getGoogleOAuthTokens({ code }: { code: string; }): Promise<IGoogleTokensResult> {
    const url = "https://oauth2.googleapis.com/token";

    const values = {
        code,
        client_id: config.get("googleClientId"),
        client_secret: config.get("googleClientSecret"),
        redirect_uri: config.get("googleOauthRedirectUrl"),
        grant_type: "authorization_code",
    };

    try {
        const res = await axios.post<IGoogleTokensResult>(
            url,
            qs.stringify(values),
            {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
            }
        )

        return res.data;

    } catch (error: any) {
        console.error(error.response.data.error);
        log.error(error, "Failed to fetch Google Oauth Tokens");
        throw new Error(error.message);
    }
}


export async function getGoogleUser({ id_token, access_token }: IKeysPair): Promise<IGoogleUserResult> {
    try {
        const res = await axios.get<IGoogleUserResult>(
            `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${access_token}`,
            {
                headers: {
                    Authorization: `Bearer ${id_token}`,
                },
            }
        );
        return res.data;
    } catch (error: any) {
        log.error(error, "Error fetching Google user");
        throw new Error(error.message);
    }
}

export async function findAndUpdateUser(
    query: FilterQuery<UserDocument>,
    update: UpdateQuery<UserDocument>,
    options: QueryOptions = {}
) {
    return UserModel.findOneAndUpdate(query, update, options);
}

export async function getCurrentUser(req: Request, res: Response) {
    return res.send(res.locals.user);
}

export async function connectHost(req: Request, res: Response) {
    const code = req.body.code

    const record = await ConnectionModel.findOne({code}).exec()
    if (!record) {
        res.status(401).send('Wrong code');
        return
    }

    const host = await HostModel.findOne({id: record["host_id"]}).exec()

    if (!host) {
        res.status(500).send('Host is missing');
        return
    }

    await UserModel.findOneAndUpdate(
        {id: res.locals.user.id},
        { $addToSet: { hosts: host._id } },
        { new: true, useFindAndModify: false }
    ).exec().catch(() => {
        res.status(500).send('An error occurred while connecting');
    });

    res.status(200).send();
}

export async function getHosts(req: Request, res: Response) {
    const user = await UserModel.findOne({id: res.locals.user.id})
        .populate('hosts')
        .exec()
        .catch(() => res.status(500).send())

    // @ts-ignore
    res.status(200).json(user?.hosts)
}
