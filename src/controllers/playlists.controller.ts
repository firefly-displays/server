import {Request, Response} from "express";
import {wsRequest} from "../utils/wsRequest";

export async function getPlaylists(req: Request, res: Response) {
    const msg = {
        type: 'get',
        entity: 'queue',
        id: '*'
    }
    wsRequest(res, msg)
}

export async function postPlaylist(req: Request, res: Response) {
    const msg = {
        type: 'post',
        entity: 'queue',
        payload: JSON.stringify({
            name: req.body.name,
            mediaIds: req.body.mediaIds
        })
    }
    console.log(msg)

    wsRequest(res, msg)
}

