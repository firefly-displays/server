import express from "express";
import config from "config";
import cors from "cors";
import cookieParser from "cookie-parser";
import connect from "./utils/connect";
import logger from "./utils/logger";
import routes from "./routes";
import {deserializeUser, deserializeHost} from "./middleware/deserializeUser";
import { createServer } from "http";
import WebSocket from 'ws';
import {onWSConnection, verifyClient} from "./controllers/ws.controller";

const port = config.get<number>("port");

const app = express();
const httpServer = createServer(app);
const wss = new WebSocket.Server({
    server: httpServer,
    verifyClient: verifyClient
})

app.use(cors({
    origin: config.get("origin"),
    credentials: true,
}));

app.use(cookieParser());
app.use(express.json());
app.use(deserializeUser);

export type GlobalType = typeof globalThis & {
    hosts: { [key: string]: { connection: WebSocket, client: WebSocket|null } }
}

(global as GlobalType).hosts = {};

wss.on('connection', onWSConnection)

httpServer.listen(port, async () => {
    logger.info(`App is running at http://localhost:${port}!`);
    await connect();
    routes(app);
});
