import { Express, Request, Response } from "express";
import requireUser from "./middleware/requireUser";
import {authHost, googleOauthHandler, logout} from "./controllers/auth.controller";
import {connectHost, getCurrentUser, getHosts} from "./controllers/user.controller";
import requireHost from "./middleware/requireHost";
import {getHostConnectionCode} from "./controllers/host.controller";
import {deserializeHost, deserializeUser} from "./middleware/deserializeUser";
import requireHostWS from "./middleware/requireHostWS";
import {getDisplays} from "./controllers/displays.controller";
import {getShedulers, postScheduler} from "./controllers/shedulers.controller";
import {getMedia, getThumb} from "./controllers/media.controller";

function routes(app: Express) {
    app.get("/healthcheck", (req: Request, res: Response) => res.sendStatus(200));
    app.get("/api/me", requireUser, getCurrentUser);
    app.get("/api/oauth/google", googleOauthHandler);
    app.get("/api/logout", logout);
    app.post("/api/authHost", authHost);
    app.get('/api/host/code', deserializeHost, requireHost, getHostConnectionCode)
    app.post('/api/connectHost', requireUser, connectHost)
    app.get('/api/hosts', requireUser, getHosts)

    app.get('/api/displays', requireUser, requireHostWS, getDisplays)

    app.get('/api/schedulers', requireUser, requireHostWS, getShedulers)
    app.post('/api/schedulers', requireUser, requireHostWS, postScheduler)

    app.get('/api/media', requireUser, requireHostWS, getMedia)
    app.get('/api/mediathumb', requireUser, requireHostWS, getThumb)
}

export default routes;
