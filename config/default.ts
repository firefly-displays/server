export default {
    port: 1337,
    origin: "http://localhost:3000",
    dbUri: "mongodb://mongo:27017/firefly_displays",
    saltWorkFactor: 10,
    accessTokenTtl: "15m",
    refreshTokenTtl: "1y",
    publicKey: `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1KWbYQRiX7wwoOnCTSZ0
ZJK1kxGcwLeMTv3zJ1CZ16lt+xH+mMJtOc0shmjPquJz6F3AFRWJo1wCcc9yWpvd
eTgKQpXC1afk9ptX/zKmLmRC/Qkf0EYNQTfrt0Ia9cen8HqOwdWiXh8Tqje03E0y
NcflHG4Y5z3+fCyFa9RhAK3qyJy2uXOXTTNBUZRPaLkle+mBx5jBwmt9RE42szsO
Eev9K7E21AvVo+cW+PRJOgwwEgIGd1vojadUdy54marJDvkxLg0XBz1AKVpBkJft
AkSGQi6c+tcziYLLjc55wwhWDsTHJF06bYGiO4G4BnVKBwkLw3CoND98UdTTcJKC
lQIDAQAB
-----END PUBLIC KEY-----`,
    privateKey: `-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA1KWbYQRiX7wwoOnCTSZ0ZJK1kxGcwLeMTv3zJ1CZ16lt+xH+
mMJtOc0shmjPquJz6F3AFRWJo1wCcc9yWpvdeTgKQpXC1afk9ptX/zKmLmRC/Qkf
0EYNQTfrt0Ia9cen8HqOwdWiXh8Tqje03E0yNcflHG4Y5z3+fCyFa9RhAK3qyJy2
uXOXTTNBUZRPaLkle+mBx5jBwmt9RE42szsOEev9K7E21AvVo+cW+PRJOgwwEgIG
d1vojadUdy54marJDvkxLg0XBz1AKVpBkJftAkSGQi6c+tcziYLLjc55wwhWDsTH
JF06bYGiO4G4BnVKBwkLw3CoND98UdTTcJKClQIDAQABAoIBADIiNSbDXNoLST+2
vOtphIltmUdqIJXC8F5zOs9rDkotFNPbozWqqQu2lqe9sDAuoXsIF1AhOsj72Uux
x9ka5DU/BBQ8sKmkorONXkq5SzqmwrBbZVe6HEfjAjy7ZhXKE4r7qgqC7nvYQc+g
wJAr1wmc7iQsxMxH7maiLHaGpLP+3nzEwyuM/8vqQvb7P1QhHUcEyauJd9THkuJj
s9FgqLYcU/b+33aTXKGSaX9nnj6bwM4bZP6yKZgAd894UZYZWNA8XZCpFZdBMLNh
fpgG7RrvGRLDd0YDzCmJlUdSEZL2UUu3aAvwpF3clovme0+YwEJ/XIC05wMaQ/rQ
V9Q8caECgYEA/LNVLck+bHHucZbTlWBhSWwjfxwf0S1xhDzwISHRAXkOZ7Du4fiH
yC6EeyPUjvqbkXtgb/V5wdmVSbq8AsI3vgW9yr2eHWNGaA2wlIkdeZC7UeD+Z2Ps
OWh882TUs1xIRef98LlY3SrPt54JvksHT0TR7ilhNwHm5+/8CKVsPGkCgYEA12xk
eyopWSq6+uLtjlGRk9qWZc90+Ur5jCye0GBpzJjINZNdtA19RcrlAHCEnnVrjypa
SrzykNaqHF20zLhNPDfiz8gFOxSZvxjClCr1nnlnEsfv8IHoZ/CDs6lGxUYx5qnS
bEID4PSKxl8nbriYafIfAinJEFZLifFXYagiv00CgYA7uxwANZhmw8tr2A3tOhnk
5AXD7t7qb8ZsTocoHbkskvO/uMfObBOUUsKH5OFCZqRNEA+shyaflpG+GsCEGmpY
pTzGZmmwYp3qzImXoo1wBbDxWrBnuevFahhW49ePCdofhAW3V+hUZuEwcs+0xkFV
EFxa4jcTbISrAEPfu1JJOQKBgQCP6eO+vE3fOcKsNMnK2AdPJSpwXYWhvUq1MGHw
h1hmzHKECC0DSje0VnViM8kFKyBuBjdH0q6hwCboyFkuorlGT3eAo/mKnJqRpXLo
FUL9WdFYJKXQoJioR29UqPOUeu5/UFg45mk88WF8YLkxKLT5ojzA7zVZaq0QPGDJ
5s8/yQKBgQDpbqk9AJocks6sXSxcVqrVr8dx9RMD52q3aBtxNjCtb3RiZgvMVZ8+
tgShD1JCu0KT/K5REqbU5fqloEXeKV5yER4UgZxv9HFj5pqOFlDp9fiH15ne7J1A
1wY71SDjO5Lh5FH7EMvb/AafQE4+uQvUDO5CZtgaoiYmXt9Ql1d5OQ==
-----END RSA PRIVATE KEY-----`,
    googleClientId: "840570424789-f2fbnv7et41k789e6kdn3prsek41uj3c.apps.googleusercontent.com",
    googleClientSecret: "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",
    googleOauthRedirectUrl: "http://oneren.space:1337/api/oauth/google",
};
